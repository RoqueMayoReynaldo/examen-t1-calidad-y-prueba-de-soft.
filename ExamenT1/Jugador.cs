﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenT1
{
    public class Jugador
    {
        public String nombre { get; set; }
        public List<Carta> cartas { get; set; }
        
    }
}
