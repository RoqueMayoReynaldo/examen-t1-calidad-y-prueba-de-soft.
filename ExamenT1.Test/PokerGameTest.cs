﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenT1.Test
{
    class PokerGameTest
    {
        [Test]
        public void jugarPokerGameCaso01()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };
            Jugador player3 = new Jugador() { nombre = "Raul" };
            Jugador player4 = new Jugador() { nombre = "Jose" };
            Jugador player5 = new Jugador() { nombre = "Perez" };
            Jugador player6 = new Jugador() { nombre = "Matilda" };



            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);
            pokerGame.addJugador(player3);
            pokerGame.addJugador(player4);
            pokerGame.addJugador(player5);
            pokerGame.addJugador(player6);


            Assert.Throws(typeof(Exception), () => pokerGame.getJugadoresGame());

        }



        [Test]
        public void jugarPokerGameCaso02()
        {
            PokerGame pokerGame = new PokerGame();

            Jugador player1 = new Jugador() { nombre = "Rosa" };

            Assert.Throws(typeof(Exception), () => pokerGame.getJugadoresGame());


        }
      

        [Test]
        public void jugarPokerGameCaso03()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };
            Jugador player3 = new Jugador() { nombre = "Raul" };
            Jugador player4 = new Jugador() { nombre = "Jose" };
            Jugador player5 = new Jugador() { nombre = "Perez" };
           



            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);
            pokerGame.addJugador(player3);
            pokerGame.addJugador(player4);
            pokerGame.addJugador(player5);
      

            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=11},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=11},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Rombo",numero=1},
               new Carta { simbolo="Espada",numero=6},
               new Carta { simbolo="Espada",numero=11}
            };

            List<Carta> cartas3 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=11},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Rombo",numero=2},
               new Carta { simbolo="Espada",numero=12},
               new Carta { simbolo="Espada",numero=6}
            };
            List<Carta> cartas4 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=11},
               new Carta { simbolo="Trebol",numero=9},
               new Carta { simbolo="Rombo",numero=12},
               new Carta { simbolo="Espada",numero=10},
               new Carta { simbolo="Espada",numero=4}
            };
            List<Carta> cartas5 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=1},
               new Carta { simbolo="Trebol",numero=3},
               new Carta { simbolo="Rombo",numero=5},
               new Carta { simbolo="Espada",numero=5},
               new Carta { simbolo="Espada",numero=12}
            };
      

            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas2);
            pokerGame.repartirCartas(player3, cartas3);
            pokerGame.repartirCartas(player4, cartas4);
            pokerGame.repartirCartas(player5, cartas5);
         



            Assert.Throws(typeof(Exception), () => pokerGame.getCartaJugadores());

        }


        [Test]
        public void jugarPokerGameCaso04()
        {
            PokerGame pokerGame = new PokerGame();

            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };
        

            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);


            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=6},
               new Carta { simbolo="Corazon",numero=8},
               new Carta { simbolo="Rombo",numero=1},
               new Carta { simbolo="Corazon",numero=2},
               new Carta { simbolo="Trebol",numero=4}
            };

     

            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas2);
            

            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player1.nombre, winPlayer);


        }


        [Test]
        public void jugarPokerGameCaso05()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };
            Jugador player3 = new Jugador() { nombre = "Raul" };



            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);
            pokerGame.addJugador(player3);


            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=6},
               new Carta { simbolo="Corazon",numero=8},
               new Carta { simbolo="Rombo",numero=9},
               new Carta { simbolo="Corazon",numero=2},
               new Carta { simbolo="Trebol",numero=4}
            };

            List<Carta> cartas3 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=5},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Rombo",numero=1},
               new Carta { simbolo="Corazon",numero=3},
               new Carta { simbolo="Trebol",numero=7}
            };

            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);
            pokerGame.repartirCartas(player3, cartas1);

            pokerGame.retirarJugador(player1);


            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player3.nombre, winPlayer);


        }
      

        [Test]
        public void jugarPokerGameCaso06()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };
            Jugador player3 = new Jugador() { nombre = "Raul" };



            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);
            pokerGame.addJugador(player3);


            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=6},
               new Carta { simbolo="Corazon",numero=8},
               new Carta { simbolo="Rombo",numero=1},
               new Carta { simbolo="Corazon",numero=2},
               new Carta { simbolo="Trebol",numero=4}
            };

            List<Carta> cartas3 = new List<Carta>()
            {
               new Carta { simbolo="Rombo",numero=9},
               new Carta { simbolo="Rombo",numero=10},
               new Carta { simbolo="Rombo",numero=11},
               new Carta { simbolo="Rombo",numero=12},
               new Carta { simbolo="Rombo",numero=13}
            };

            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);
            pokerGame.repartirCartas(player3, cartas1);

            pokerGame.retirarJugador(player1);
            pokerGame.retirarJugador(player3);

            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player2.nombre, winPlayer);


        }

        [Test]
        public void jugarPokerGameCaso07()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador();
       


            pokerGame.addJugador(player1);


            Assert.Throws(typeof(Exception), () => pokerGame.getJugadoresGame());


        }


        [Test]
        public void jugarPokerGameCaso08()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };
            Jugador player3 = new Jugador() { nombre = "Raul" };
            Jugador player4 = new Jugador() { nombre = "Jose" };
            Jugador player5 = new Jugador() { nombre = "Perez" };
            Jugador player6 = new Jugador() { nombre = "Matilda" };
            Jugador player7 = new Jugador() { nombre = "Ivan" };
            Jugador player8 = new Jugador() { nombre = "Estela" };
            Jugador player9 = new Jugador() { nombre = "Maria" };
            Jugador player10 = new Jugador() { nombre = "Juana" };
            Jugador player11 = new Jugador() { nombre = "Margarita" };
            Jugador player12= new Jugador() { nombre = "Ines" };



            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);
            pokerGame.addJugador(player3);
            pokerGame.addJugador(player4);
            pokerGame.addJugador(player5);
            pokerGame.addJugador(player6);
            pokerGame.addJugador(player7);
            pokerGame.addJugador(player8);
            pokerGame.addJugador(player9);
            pokerGame.addJugador(player10);
            pokerGame.addJugador(player11);
            pokerGame.addJugador(player12);

            Assert.Throws(typeof(Exception), () => pokerGame.getJugadoresGame());



        }


        [Test]
        public void jugarPokerGameCaso09()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };
           



            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10},
               new Carta { simbolo="Trebol",numero=9}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=14},
               new Carta { simbolo="Corazon",numero=13},
               new Carta { simbolo="Corazon",numero=12},
               new Carta { simbolo="Corazon",numero=11},
               new Carta { simbolo="Corazon",numero=10}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);
         



            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player2.nombre, winPlayer);


        }



        [Test]
        public void jugarPokerGameCaso10()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Joker",numero=0},
               new Carta { simbolo="Corazon",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Rombo",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player1.nombre, winPlayer);


        }


        [Test]
        public void jugarPokerGameCaso11()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Rombo",numero=11},
               new Carta { simbolo="Rombo",numero=10},
               new Carta { simbolo="Rombo",numero=9},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Rombo",numero=7}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player1.nombre, winPlayer);


        }




        [Test]
        public void jugarPokerGameCaso12()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Rombo",numero=3},
               new Carta { simbolo="Rombo",numero=9},
               new Carta { simbolo="Trebol",numero=9},
               new Carta { simbolo="Espada",numero=9},
               new Carta { simbolo="Corazon",numero=9}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player1.nombre, winPlayer);


        }

        [Test]
        public void jugarPokerGameCaso13()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Espada",numero=3},
               new Carta { simbolo="Trebol",numero=3},
               new Carta { simbolo="Corazon",numero=6},
               new Carta { simbolo="Espada",numero=6},
               new Carta { simbolo="Rombo",numero=6}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player1.nombre, winPlayer);


        }




        [Test]
        public void jugarPokerGameCaso14()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=4},
               new Carta { simbolo="Corazon",numero=14},
               new Carta { simbolo="Corazon",numero=11},
               new Carta { simbolo="Corazon",numero=7},
               new Carta { simbolo="Corazon",numero=2}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player1.nombre, winPlayer);


        }

        [Test]
        public void jugarPokerGameCaso15()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=7},
               new Carta { simbolo="Espada",numero=6},
               new Carta { simbolo="Rombo",numero=5},
               new Carta { simbolo="Trebol",numero=4},
               new Carta { simbolo="Rombo",numero=3}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player1.nombre, winPlayer);


        }



        [Test]
        public void jugarPokerGameCaso16()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Rombo",numero=10},
               new Carta { simbolo="Trebol",numero=2},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player1.nombre, winPlayer);


        }


        [Test]
        public void jugarPokerGameCaso17()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Espada",numero=5},
               new Carta { simbolo="Trebol",numero=2},
               new Carta { simbolo="Corazon",numero=5},
               new Carta { simbolo="Espada",numero=12},
               new Carta { simbolo="Rombo",numero=12}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player1.nombre, winPlayer);


        }


        [Test]
        public void jugarPokerGameCaso18()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=11},
               new Carta { simbolo="Espada",numero=2},
               new Carta { simbolo="Trebol",numero=7},
               new Carta { simbolo="Rombo",numero=13},
               new Carta { simbolo="Trebol",numero=13}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);


            Assert.Throws(typeof(Exception), () => pokerGame.getCartaJugadores());

  


        }


        [Test]
        public void jugarPokerGameCaso19()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Trebol",numero=14},
               new Carta { simbolo="Trebol",numero=13},
               new Carta { simbolo="Trebol",numero=12},
               new Carta { simbolo="Trebol",numero=11},
               new Carta { simbolo="Trebol",numero=10}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Espada",numero=10},
               new Carta { simbolo="Trebol",numero=3},
               new Carta { simbolo="Corazon",numero=5},
               new Carta { simbolo="Espada",numero=7},
               new Carta { simbolo="Rombo",numero=12}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player1.nombre, winPlayer);


        }


        [Test]
        public void jugarPokerGameCaso20()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Rombo",numero=11},
               new Carta { simbolo="Rombo",numero=10},
               new Carta { simbolo="Rombo",numero=9},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Rombo",numero=7}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Joker",numero=0},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            Assert.Throws(typeof(Exception), () => pokerGame.getCartaJugadores());


        }



        [Test]
        public void jugarPokerGameCaso21()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Rombo",numero=3},
               new Carta { simbolo="Rombo",numero=9},
               new Carta { simbolo="Trebol",numero=9},
               new Carta { simbolo="Espada",numero=9},
               new Carta { simbolo="Corazon",numero=9}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Joker",numero=0},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player2.nombre, winPlayer);


        }



        [Test]
        public void jugarPokerGameCaso22()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Espada",numero=3},
               new Carta { simbolo="Trebol",numero=3},
               new Carta { simbolo="Corazon",numero=6},
               new Carta { simbolo="Espada",numero=6},
               new Carta { simbolo="Rombo",numero=6}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Joker",numero=0},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player2.nombre, winPlayer);


        }



        [Test]
        public void jugarPokerGameCaso23()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=4},
               new Carta { simbolo="Corazon",numero=14},
               new Carta { simbolo="Corazon",numero=11},
               new Carta { simbolo="Corazon",numero=7},
               new Carta { simbolo="Corazon",numero=2}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Joker",numero=0},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player2.nombre, winPlayer);


        }




        [Test]
        public void jugarPokerGameCaso24()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=7},
               new Carta { simbolo="Espada",numero=6 },
               new Carta { simbolo="Rombo",numero=5},
               new Carta { simbolo="Trebol",numero=4},
               new Carta { simbolo="Rombo",numero=3}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Joker",numero=0},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player2.nombre, winPlayer);


        }

        [Test]
        public void jugarPokerGameCaso25()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Rombo",numero=10},
               new Carta { simbolo="Trebol",numero=2 },
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Joker",numero=0},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            Assert.Throws(typeof(Exception), () => pokerGame.getCartaJugadores());


        }



        [Test]
        public void jugarPokerGameCaso26()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Espada",numero=5},
               new Carta { simbolo="Trebol",numero=2 },
               new Carta { simbolo="Corazon",numero=5},
               new Carta { simbolo="Espada",numero=12},
               new Carta { simbolo="Rombo",numero=12}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Joker",numero=0},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player2.nombre, winPlayer);


        }



        [Test]
        public void jugarPokerGameCaso27()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=11},
               new Carta { simbolo="Espada",numero=2},
               new Carta { simbolo="Trebol",numero=7},
               new Carta { simbolo="Rombo",numero=13},
               new Carta { simbolo="Trebol",numero=13}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Joker",numero=0},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player2.nombre, winPlayer);


        }



        [Test]
        public void jugarPokerGameCaso28()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Espada",numero=10},
               new Carta { simbolo="Trebol",numero=3},
               new Carta { simbolo="Corazon",numero=5},
               new Carta { simbolo="Espada",numero=7},
               new Carta { simbolo="Rombo",numero=12}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Joker",numero=0},
               new Carta { simbolo="Rombo",numero=8},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player2.nombre, winPlayer);


        }


        [Test]
        public void jugarPokerGameCaso29()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=11},
               new Carta { simbolo="Espada",numero=2},
               new Carta { simbolo="Trebol",numero=7 },
               new Carta { simbolo="Rombo",numero=13},
               new Carta { simbolo="Trebol",numero=13}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Espada",numero=10},
               new Carta { simbolo="Trebol",numero=3},
               new Carta { simbolo="Corazon",numero=5},
               new Carta { simbolo="Espada",numero=7},
               new Carta { simbolo="Rombo",numero=12}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player1.nombre, winPlayer);


        }

        [Test]
        public void jugarPokerGameCaso30()
        {
            PokerGame pokerGame = new PokerGame();



            Jugador player1 = new Jugador() { nombre = "Rosa" };
            Jugador player2 = new Jugador() { nombre = "Estevan" };




            pokerGame.addJugador(player1);
            pokerGame.addJugador(player2);



            List<Carta> cartas1 = new List<Carta>()
            {
               new Carta { simbolo="Corazon",numero=11},
               new Carta { simbolo="Espada",numero=2},
               new Carta { simbolo="Trebol",numero=7 },
               new Carta { simbolo="Rombo",numero=13},
               new Carta { simbolo="Trebol",numero=13}
            };

            List<Carta> cartas2 = new List<Carta>()
            {
               new Carta { simbolo="Rombo",numero=10},
               new Carta { simbolo="Trebol",numero=2},
               new Carta { simbolo="Trebol",numero=8},
               new Carta { simbolo="Espada",numero=8},
               new Carta { simbolo="Corazon",numero=8}
            };


            pokerGame.repartirCartas(player1, cartas1);
            pokerGame.repartirCartas(player2, cartas1);




            var winPlayer = pokerGame.gamePoker();

            Assert.AreEqual(player2.nombre, winPlayer);


        }

    }
}
